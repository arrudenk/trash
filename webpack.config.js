const babelLoader = {
    loader: "babel-loader",
    options: {
        presets: [
            [
                "es2015",
                {
                    modules: false
                }
            ],
            "es2016"
        ]
    }
};

module.exports = {
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    babelLoader,
                    {
                        loader: "ts-loader"
                    }
                ]
            },
            {
                test: /\.js$/,
                use: [
                    babelLoader
                ]
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    entry: {
        game: "./src/test.ts",
    },
    output: {
        filename: "bundles/[name].js"
    },
    devtool: "inline-source-map"
};
