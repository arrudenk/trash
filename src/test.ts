import {GodrayFilter} from "@pixi/filter-godray";

export class Test {

    protected app: PIXI.Application;
    protected sprite: PIXI.Sprite;

    protected filters: any[];

    protected waterDisplacementMap: PIXI.Sprite;

    protected godraysFilter: GodrayFilter;
    protected waterFilter: PIXI.filters.DisplacementFilter;
    protected depthyFilter: PIXI.filters.DisplacementFilter;

    constructor() {
        this.app = new PIXI.Application({width: window.innerWidth, height: window.innerHeight});
        document.body.appendChild(this.app.view);

        this.sprite = PIXI.Sprite.from("./images/unknown.png");
        this.sprite.width = window.innerWidth;
        this.sprite.height = window.innerHeight;

        this.app.stage.addChild(this.sprite);
        this.app.stage.filters = [];
        this.filters = this.app.stage.filters;
    }

    public runAllFilters() {
        this.addDepthyMapFilter();
        this.addGodraysFilter();
        this.addDisplacementWaterFilter();
        this.startTicker();
    }


    addDepthyMapFilter() {
        const depthMap = PIXI.Sprite.from("./images/unknown-depthmap.jpg");

        depthMap.width = window.innerWidth;
        depthMap.height = window.innerHeight;
        this.app.stage.addChild(depthMap);

        this.depthyFilter = new PIXI.filters.DisplacementFilter(depthMap);
        this.filters.push(this.depthyFilter);

        window.onmousemove = (e) => {
            this.depthyFilter.scale.x = (window.innerWidth / 2 - e.clientX) / 20;
            this.depthyFilter.scale.y = (window.innerHeight / 2 - e.clientY) / 20;
        };
    }

    addGodraysFilter() {
        const godRayFilterOptions = {
            angle: 30,
            center: new PIXI.Point(100, -100),
            parallel: false,
            gain: 0.6,
            lacunarity: 2.8,
            time: 0
        };

        this.godraysFilter = new GodrayFilter(godRayFilterOptions);

        this.filters.push(this.godraysFilter);
    }

    addDisplacementWaterFilter() {
        this.waterDisplacementMap = PIXI.Sprite.from("./images/cloud.png");
        this.waterFilter = new PIXI.filters.DisplacementFilter(this.waterDisplacementMap);

        this.app.stage.addChild(this.waterDisplacementMap);

        this.waterDisplacementMap.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
        this.waterDisplacementMap.scale.set(4);

        this.filters.push(this.waterFilter);
    }

    waterAnimation(displacementMap: PIXI.Sprite) {
        displacementMap.x += 4;
        displacementMap.y += 2;
    }

    godraysAnimation(filter: GodrayFilter){
         filter.time += this.app.ticker.elapsedMS / 1e3;
    }

    startTicker() {
        this.app.ticker.add((delta) => {
            this.waterAnimation(this.waterDisplacementMap);
            this.godraysAnimation(this.godraysFilter);
        });
    }
}

const test = new Test();

test.runAllFilters();